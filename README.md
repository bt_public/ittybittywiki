IttyBittyWiki (IBW) is a Wiki implemented as a self contained Single Page Application (SPA) that can be run directly from the file system (no server required). All logic and presentation is contained in a single HTML file that you can drop into your repository, the wiki content is stored in a separate JSON file in the same directory and modified by the Wiki application.

The code and releases for this project are released under a Creative Commons [Attribution-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/) license. 

The full documentation is available through [IBW itself](https://bt_public.gitlab.io/ittybittywiki).

# Why Another Wiki?

IBW was designed to allow developer centric documentation for a project to be easily updated and tracked along with the source files. I originally wrote IBW to solve some specific annoyances:

* Although hosted source control systems provide their own Wiki implementations it can be a problem migrating from one provider to another and transferring the Wiki content. Self hosted, internal repositories may be using simpler tools that simply don't provide the functionality.
* Wiki functionality provided by GitLab, GitHub and others are an extension to the repository and not part of it. If you check out an older version of your project with different installation instructions it can be difficult to find the matching instructions in the Wiki history. Because the Wiki content for IBW is simply another file in the repository the content will match the code.
* I often work offline, either because internet is not available or I have disconnected to allow me to focus, and still wanted to be able to update the content. IBW runs directly from the file system (just double click the HTML file) and has no external dependencies.
