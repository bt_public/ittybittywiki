# Building

From a command prompt in this directory run `npm install` and then ...

* Development builds - `npm run dev`
* Production builds - `npm run build`

Both processes will result in `dist/index.html`, open this file directly in the browser for testing.
