'use strict';

// Plugins
const webpack                 = require('webpack');
const MiniCSSExtractPlugin    = require('mini-css-extract-plugin');
const { CleanWebpackPlugin }  = require('clean-webpack-plugin');

// Helpers
const helpers              = require('./helpers');
const isDev                = process.env.NODE_ENV === 'development';

const webpackConfig = {
    entry: helpers.root('src', 'main'),
    resolve: {
        extensions: [ '.js' ],
        alias: {
            '@': helpers.root('src')
        }
    },       
    devtool: 'source-map',
    performance: {
      // Turn off size warnings for entry points
      hints: false,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [ helpers.root('src') ]
            },
            {
                test: /\.css$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDev } },
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDev } },
                    { loader: 'sass-loader', options: { sourceMap: isDev } }
                ]
            },
            {
                test: /\.sass$/,
                use: [
                    MiniCSSExtractPlugin.loader,
                    { loader: 'css-loader', options: { sourceMap: isDev } },
                    { loader: 'sass-loader', options: { sourceMap: isDev } }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCSSExtractPlugin({
            path: helpers.root('dist'),
            filename: 'bundle.css'
        }),
        new webpack.DefinePlugin({
            __VERSION__: JSON.stringify(process.env.BUILD_VERSION)
         })
    ]
};

module.exports = webpackConfig;
