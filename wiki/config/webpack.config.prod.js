'use strict';

// Core
const webpack                  = require('webpack');
const merge                    = require('webpack-merge');

// Plugins
const HtmlWebpackPlugin        = require('html-webpack-plugin');
const HtmlWebpackInlinePlugin  = require('html-webpack-inline-source-plugin');
const TerserJSPlugin           = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin  = require('optimize-css-assets-webpack-plugin');

// Helpers
const helpers                  = require('./helpers');
const commonConfig             = require('./webpack.config.common');
const isProd                   = process.env.NODE_ENV === 'production';
const environment              = isProd ? require('./env/prod.env') : require('./env/staging.env');

const webpackConfig = merge(commonConfig, {
    mode: 'production',
    output: {
        filename: 'bundle.js',
        path: helpers.root('dist')
    },
    optimization: {
        minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    },    
    plugins: [
        new webpack.EnvironmentPlugin(environment),
        new HtmlWebpackPlugin({
            template: helpers.root('index.html'),
            inlineSource: '.(js|css)$' // embed all javascript and css inline
        }),
        new HtmlWebpackInlinePlugin(),
    ],
});

if (!isProd) {
    webpackConfig.devtool = 'source-map';

    if (process.env.npm_config_report) {
        const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
        webpackConfig.plugins.push(new BundleAnalyzerPlugin());
    }
}

module.exports = webpackConfig;
