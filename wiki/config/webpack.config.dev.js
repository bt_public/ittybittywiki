'use strict';

// Core
const webpack              = require('webpack');
const merge                = require('webpack-merge');

// Plugins
const HtmlWebpackPlugin    = require('html-webpack-plugin');

// Helpers
const helpers              = require('./helpers');
const commonConfig         = require('./webpack.config.common');
const environment          = require('./env/dev.env');

const webpackConfig = merge(commonConfig, {
    mode: 'development',
    output: {
        path: helpers.root('dist'),
        filename: '[name].bundle.js',
        chunkFilename: '[id].chunk.js'
    },
    plugins: [
        new webpack.EnvironmentPlugin(environment),
        new HtmlWebpackPlugin({
            template: helpers.root('index.html'),
            inject: true
        })
    ],
});

module.exports = webpackConfig;
