/*--------------------------------------------------------------------------*
* Entry point for Itty Bitty Wiki - does main application set.
*--------------------------------------------------------------------------*/

// Set up styles
import 'bulma/css/bulma.css';
import '@/css/default.css';

// Import knockout
import ko from 'knockout';
import ViewModel from '@/libs/viewmodel';
import WikiDoc from '@/libs/wikidoc';
import ViewState from '@/libs/viewstate';
import Navigate from '@/libs/navigate';

/** Helper to set the page according to the hash
 */
function switchPageToHash() {
  let slug = window.location.hash.substr(1);
  if (ViewModel.page().slug() == slug)
    return; // Do nothing
  // Switch the page
  Navigate.navigateTo(slug);
  window.location.hash = ViewModel.page().slug();
}

// Set up hash change event
window.addEventListener("hashchange", (data, ev) => {
  switchPageToHash();
  },
  false);

console.log("Starting application");
WikiDoc.loadDocument().then(
  (doc) => {
    // Set up state
    ViewModel.wikiDoc(doc);
    ViewModel.pages(doc.raw.pages);
    WikiDoc.onNewPage.push((p) => {
      ViewModel.pages(doc.raw.pages);
    });
    switchPageToHash();
    ko.applyBindings(ViewModel);
  }
).catch(
  (err) => {
    console.log("Error: " + err);
    ko.applyBindings(ViewModel);
  }
);
