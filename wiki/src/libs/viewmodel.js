/*--------------------------------------------------------------------------*
* Manage view model
*--------------------------------------------------------------------------*/

// Import knockout
import ko from 'knockout';
import moment from 'moment';
import WikiDoc from '@/libs/wikidoc';
import markdown from '@/libs/markdown';

// Initial view model
let ViewModel = { };

//--- Set up the view model

// Version
ViewModel.version = WikiDoc.PRODUCT_VERSION;

// Set when the page has been loaded
ViewModel.loaded = ko.observable(true);

// The full wiki document
ViewModel.wikiDoc = ko.observable(null);

// List of pages
ViewModel.pages = ko.observableArray();

// Project name
ViewModel.projectName = ko.computed(() => {
  if (ViewModel.wikiDoc() != null)
    return ViewModel.wikiDoc().raw.project;
  return "IttyBittyWiki";
});

// Current page
ViewModel.page = ko.observable({
  object: ko.observable(null),
  title: ko.observable(null),
  slug: ko.observable(null),
  text: ko.observable(null),
  html: ko.observable(null),
  created: ko.observable(null),
  modified: ko.observable(null)
});

/** Update the viewmodel with the given page object
 */
ViewModel.updateCurrentPage = function (page) {
  ViewModel.page().object(page);
  ViewModel.page().title(page.title);
  ViewModel.page().slug(page.slug);
  ViewModel.page().text(page.markdown.join("\n"));
  ViewModel.page().html(markdown.toHtml(ViewModel.wikiDoc(), ViewModel.page().text()));
  ViewModel.page().created(moment(page.created).format("MMM Do, YYYY @ HH:mm"));
  ViewModel.page().modified(moment(page.modified).format("MMM Do, YYYY @ HH:mm"));
}

/** Update view model with the selected page
 */
ViewModel.selectPage = (slug) => {
  let page = WikiDoc.findPageBySlug(ViewModel.wikiDoc(), slug);
  if (page == null) // Fall back to default
    page = WikiDoc.findPageBySlug(ViewModel.wikiDoc(), WikiDoc.DEFAULT_PAGE);
  // Update current page details
  ViewModel.updateCurrentPage(page);
};

// Download link
ViewModel.triggerDocumentDownload = (data, event) => {
  if (ViewModel.wikiDoc() == null)
    return; // TODO: Should report an error here
  // Create the blob and URL instance
  let url = URL.createObjectURL(
    new Blob(
      [ "var wikidoc =\n" + WikiDoc.generateDocument(ViewModel.wikiDoc()) + "\n;" ],
      { type: "application/javascript" }
  ));
  // Create an anchor and trigger the download automatically
  const a = document.createElement('a');
  a.href = url;
  a.download = 'wikidoc.js';
  // Click handler that releases the object URL after the element has been clicked
  const clickHandler = () => {
    setTimeout(() => {
      URL.revokeObjectURL(url);
      a.removeEventListener('click', clickHandler);
    }, 150);
  };
  a.addEventListener('click', clickHandler, false);
  a.click();
  // Assume the changes have been saved
  ViewModel.modified(false);
};

// Export it
export default ViewModel;
