/*--------------------------------------------------------------------------*
* Manage view state
*--------------------------------------------------------------------------*/

// Imports
import ko from 'knockout';
import moment from 'moment';
import ViewModel from '@/libs/viewmodel';
import WikiDoc from '@/libs/wikidoc';

// Constants
const LIST_RECENT = "recent";
const LIST_HISTORY = "history";

// Set when the page has been edited
ViewModel.modified = ko.observable(false);

// Current mode
ViewModel.editMode = ko.observable(false);

// Current list
ViewModel.viewList = ko.observable(LIST_HISTORY);

//--- View model methods

/** Switch the edit mode (optionally saving changes)
 */
ViewModel.setEditMode = function (allowEdit, saveChanges) {
  // If no parameter provided assume false
  if ((allowEdit == undefined) || (allowEdit == null))
    allowEdit = false;
  if ((saveChanges == undefined) || (saveChanges == null))
    saveChanges = false;
  // Set the model property
  if (allowEdit != ViewModel.editMode()) {
    if (!allowEdit) {
      // Anything changed?
      if (saveChanges) {
        // We have changes, update the root document
        WikiDoc.addAlias(ViewModel.page().object(), ViewModel.page().object().title);
        ViewModel.page().object().title = ViewModel.page().title();
        ViewModel.page().object().markdown = ViewModel.page().text().split(/\r?\n/);
        ViewModel.page().object().modified = moment().format();
        ViewModel.modified(true);
        // Trigger changes for page list
        ViewModel.pages.valueHasMutated();
      }
      // Update the model (which may revert to previous values)
      ViewModel.updateCurrentPage(ViewModel.page().object());
    }
    // Switch mode
    ViewModel.editMode(allowEdit);
  }
};

/** Switch the list view
 */
ViewModel.setListView = function (view) {
  if ((view == LIST_RECENT) || (view == LIST_HISTORY))
    ViewModel.viewList(view);
}

//--- Exports
export default {
  // No exports
}