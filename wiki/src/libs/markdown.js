/*--------------------------------------------------------------------------*
* Customised markdown
*--------------------------------------------------------------------------*/
import WikiDoc from '@/libs/wikidoc';

//--- Helpers
const md = require('markdown-it')({
  html:        false,       // Enable HTML tags in source
  xhtmlOut:    true,        // Use '/' to close single tags (<br />).
  breaks:      true,        // Convert '\n' in paragraphs into <br>
  langPrefix:  'language-', // CSS language prefix for fenced blocks. Can be
  linkify:     true,        // Autoconvert URL-like text to links
  typographer: true,        // Enable some language-neutral replacement + quotes beautification
  quotes:      '“”‘’',      // Quote pairs
  // Highlighter function. Should return escaped HTML,
  // or '' if the source string is not changed and should be escaped externally.
  // If result starts with <pre... internal wrapper is skipped.
  highlight: function (/*str, lang*/) { return ''; }
});

//--- Convert link output to open in a new tab

// Remember old renderer, if overridden, or proxy to default renderer
var defaultLinkRender = md.renderer.rules.link_open || function(tokens, idx, options, env, self) {
  return self.renderToken(tokens, idx, options);
};

md.renderer.rules.link_open = function (tokens, idx, options, env, self) {
  // See if the href is local or external
  var aIndex = tokens[idx].attrIndex('href');
  if ((aIndex < 0) || (tokens[idx].attrs[aIndex][1].substring(0, 1) != '#')) {
    // Internal link, add or change 'target' attribute
    aIndex = tokens[idx].attrIndex('target');
    if (aIndex < 0)
      tokens[idx].attrPush(['target', '_blank']);
    else
      tokens[idx].attrs[aIndex][1] = '_blank';
  }
  // pass token to default renderer.
  return defaultLinkRender(tokens, idx, options, env, self);
};

//--- Preprocess wiki links (like [[Page Name]]) into normal links

/** Preprocess wiki links, convert to normal style links
 */
function processWikiLinks(doc, text) {
  return text.replace(
    /\[\[[a-z]+[^\[\]]*\S\]\]/ig,
    (target) => {
      let name = target.substring(2, target.length - 2);
      let page = WikiDoc.findPageByTitle(doc, name, true);
      let link = '#' + page.slug;
      return `[${name}](${link})`;
    }
  )
}

/** Convert the markdown provided into HTML using the document set
 */
function markdownToHtml(doc, text) {
  return md.render(processWikiLinks(doc, text));
}

//--- Exports
export default {
  toHtml: markdownToHtml
}