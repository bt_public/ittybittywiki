/*--------------------------------------------------------------------------*
* Manage navigation and history
*--------------------------------------------------------------------------*/

//--- Imports
import ko from 'knockout';
import moment from 'moment';
import ViewModel from '@/libs/viewmodel';

//--- Constants
const DYNAMIC_LIST_SIZE = 10;

/** Set of viewed pages
 */
ViewModel.viewed = ko.observableArray();

// Recent pages
ViewModel.recent = ko.pureComputed(function () {
  let result = ViewModel.pages.sorted(function (left, right) {
      return moment(right.modified).valueOf() - moment(left.modified).valueOf()
  });
  result.splice(DYNAMIC_LIST_SIZE, result.length - DYNAMIC_LIST_SIZE);
  return result;
});

/** Navigate to the page with the given slug
 */
function navigateTo(slug) {
  ViewModel.selectPage(slug);
  // Add the page to the top of the 'history' list
  let page = ViewModel.page().object();
  ViewModel.viewed.unshift(page);
  // Remove any duplicates
  for (var i = 1; i < ViewModel.viewed().length; i++) {
    if (ViewModel.viewed()[i].slug == page.slug) {
      ViewModel.viewed.splice(i, 1);
      break;
    }
  }
  // Trim the list
  ViewModel.viewed.splice(DYNAMIC_LIST_SIZE, ViewModel.viewed().length - DYNAMIC_LIST_SIZE);
}

//--- Exports
export default {
  navigateTo: navigateTo
};
