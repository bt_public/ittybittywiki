/*--------------------------------------------------------------------------*
* Manage the wiki document (wiki.json)
*--------------------------------------------------------------------------*/
import moment from 'moment';
import slugify from 'slugify';

//--- Constants
const DEFAULT_PAGE_NAME = "Home";
const DEFAULT_PAGE_SLUG = "home";
const DEFAULT_VERSION   = "0.0.0";
const PRODUCT_VERSION   = (__VERSION__ == undefined) ? DEFAULT_VERSION : __VERSION__;

//--- Event listeners (new page creation)
let onNewPage = [];

//--- Helpers

/** Take a list in 'source' and return a sorted, unique version of it
 */
function createUniqueList(source) {
  return source.sort().filter((value, index, array) => {
    return (index === 0) || (value !== array[index-1]);
  });
}

/** Create a 'slug' (URL friendly unique name) from a title
 */
function pageSlugFromTitle(title) {
  return slugify(title, { 
    replacement: '-',
    remove: /[*+~.()'"!:@#]/g,
    lower: true
  });
}

/** Create and return a new page instance with the given title
 * 
 * Content is optional, if nothing is provided a suitable default will
 * be provided.
 */
function createNewPage(title, content) {
  if ((content === undefined) || (content === null))
    content = "This is a new page. You should add some content here.";
  return { 
    'title': title, 
    'alias': [ title ],
    'slug': pageSlugFromTitle(title),
    'created': moment().format(),
    'modified': moment().format(),
    'markdown': content.split(/\r?\n/)
  } 
}

//--- Public API

/** Process the document
 */
async function loadDocument() {
  let rawDocument = { 
    'version': PRODUCT_VERSION,
    'project': 'IttyBittyWiki',
    'pages': [ createNewPage(DEFAULT_PAGE_NAME, "This is your default Home page which is always displayed first.") ]
  };
  if ((typeof(wikidoc) != "undefined") && (wikidoc != null)) {
    // TODO: Verify document contents and upgrade (or reject) based on version
    rawDocument = wikidoc;
  }
  // Process the document for faster lookups
  let result = { raw: rawDocument };
  result.slugs = rawDocument.pages.reduce((o, v) => ({...o, [v.slug]: v }), { });
  result.titles = rawDocument.pages.reduce((o, v) => ({...o, [v.title]: v }), { });
  // Add alias names to lookups
  rawDocument.pages.forEach((v0, i0) => {
    v0.alias = createUniqueList(v0.alias); // Fix duplicates from older doc versions.
    v0.alias.forEach((v1, i1) => {
      if (result.titles[v1] != undefined)
        result.titles[v1] = v0;
    });
  })
  // Done
  return result;
}

/** Generate a JSON serialised version of the document
 */
function generateDocument(doc) {
  doc.raw.version = PRODUCT_VERSION;
  return JSON.stringify(doc.raw, null, 2);
}

/** Find a page by it's slug
 */
function findPageBySlug(doc, slug) {
  if (doc.slugs[slug] != undefined)
    return doc.slugs[slug];
  return null;
}

/** Find a page by it's title
 */
function findPageByTitle(doc, title, create) {
  // Create should default to false
  create = (typeof(create) === "undefined") || !!create;
  // See if we have it by title
  if (doc.titles[title] != undefined)
    return doc.titles[title];
  // If we are creating, make a new one
  let newPage = null;
  if (create) {
    newPage = createNewPage(title);
    doc.raw.pages.push(newPage);
    doc.slugs[newPage.slug] = newPage;
    doc.titles[newPage.title] = newPage;
    // Fire event listeners
    onNewPage.forEach((v) => {
      try {
        v(newPage);
      }
      catch (e) {
        console.log("Error while processing newPage event listener.")
      }
    });
  }
  return newPage;
}

/** Add an alias to a page (if it is not already listed)
 */
function addAlias(page, title) {
  page.alias.push(title);
  page.alias = createUniqueList(page.alias);
}

//--- Exports
export default {
  DEFAULT_PAGE: DEFAULT_PAGE_SLUG,
  loadDocument: loadDocument,
  generateDocument: generateDocument,
  findPageBySlug: findPageBySlug,
  findPageByTitle: findPageByTitle,
  addAlias: addAlias,
  onNewPage: onNewPage,
  PRODUCT_VERSION: PRODUCT_VERSION
};
