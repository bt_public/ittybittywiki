#!/bin/sh
#----------------------------------------------------------------------------
# Helper script for CI/CD to deploy a release
# Intended to run inside our standard node-sdk container
#----------------------------------------------------------------------------
HERE=$(dirname $(readlink -f $0))
source ${HERE}/version

# Switch to artifact directory
cd ${HERE}/wiki/dist

# Verify we have the asset to deploy
echo "*** Checking for artifact"
if [ ! -f index.html ]; then
  echo "  No artifact available to deploy!!"
  exit 1
fi
cp index.html index-${VERSION_TAG}.html

# Set the tag name and add a suffix if needed
RELEASE_TAG="${VERSION_TAG}"
if [ "X${CI_COMMIT_REF_NAME}" != "Xrelease" ]; then
  RELEASE_TAG="${VERSION_TAG}-beta"
  read -r -d '' POST_BODY << EOM
{
  "tag_name": "${RELEASE_TAG}",
  "ref": "${CI_COMMIT_SHA}",
  "message": "Preview release ${VERSION_TAG}"
}
EOM
else
  # Upload the file
  echo "*** Uploading index-${VERSION_TAG}.html"
  curl -X POST --silent --show-error --fail -o response.json \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --form "file=@index-${VERSION_TAG}.html" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/uploads" || exit 1
  # Get the markdown to embed
  PACKAGE_LINK=`python -c "import sys, json; print json.load(sys.stdin)['markdown']" < response.json`
  read -r -d '' POST_BODY << EOM
{
  "tag_name": "${RELEASE_TAG}",
  "ref": "${CI_COMMIT_SHA}",
  "message": "Stable release ${RELEASE_TAG}",
  "release_description": "Stable release ${PACKAGE_LINK}"  
}
EOM
fi

# Create the release
echo "*** Create new release for ${VERSION_TAG}"
curl -X POST --silent --show-error --fail -o result.json \
  --header 'Content-Type: application/json' \
  --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
  --data "${POST_BODY}" \
  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags" || exit 1
