#!/bin/sh
#----------------------------------------------------------------------------
# Publish the current documentation with GitLab pages
#----------------------------------------------------------------------------
HERE=$(dirname $(readlink -f $0))
source ${HERE}/version

# Switch to project directory
cd ${HERE}

# Clear any existing data
if [ -d public ]; then
  rm -rf public
fi
mkdir public

# Copy the documents
cp -r docs/* public
