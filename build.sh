#!/bin/sh
#----------------------------------------------------------------------------
# Helper script for CI/CD to build everything.
# Intended to run inside our standard node-sdk container
#----------------------------------------------------------------------------
HERE=$(dirname $(readlink -f $0))
source ${HERE}/version

# Switch to source directory
cd ${HERE}/wiki

# Update modules
if [ -d node_modules ]; then
  echo "*** Removing left over artifacts"
  rm -rf node_modules || exit 1
fi
echo "*** Installing modules"
npm install || exit 1

# Build the development version
echo "*** Building target"
BUILD_VERSION=${VERSION} npm run build || exit 1
ARTIFACT_SIZE=`ls -l dist/index.html | awk '{ print $5 }'`
echo "*** Target is ${ARTIFACT_SIZE} bytes"
